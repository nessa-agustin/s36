const express = require('express');
const TaskController = require('../controller/TaskController')
const router = express.Router();

// Routes

router.get('/',(req, res) => {
    TaskController.getAllTasks()
        .then((resultFromController) => res.send(resultFromController))
    ;
})

router.post('/create',(req,res) => {

    // console.log(req.body)
    // res.send(TaskController.createTask(req.body))
  TaskController.createTask(req.body)
        .then((resultFromController) => {
            res.send(resultFromController)
        })
})


router.put('/:id/update',(req,res) => {

    TaskController.updateTask(req.params.id, req.body)
        .then((resultFromController) => res.send(resultFromController))
})

router.delete('/:id/delete',(req,res) => {
    TaskController.deleteTask(req.params.id)
        .then((resultFromController) => res.send(resultFromController))
})



// ACTIVITY
router.delete('/:id',(req,res) => {
    TaskController.getTask(req.params.id)
        .then((resultFromController) => res.send(resultFromController))
})

router.put('/:id/complete',(req,res) => {

    TaskController.completeTask(req.params.id, req.body)
        .then((resultFromController) => res.send(resultFromController))
})



module.exports = router;