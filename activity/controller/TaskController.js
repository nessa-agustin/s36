const Task = require('../models/Task')

// ACTIVITY

module.exports.getTask = (taskId) => {

    return Task.findById(taskId)
        .then((result, error) => {
            if(error){
                console.log('Task not found')
                return false
            }else{
                return result;
            }
            
        })

};

module.exports.completeTask = (taskId, reqBody) => {
    return Task.findById(taskId)
        .then((result, error) => {
            if(error){
                console.log(error)
                return false
            }else {
                result.status = reqBody.status;
                return result.save().then((updatedTask, error) => {
                    if(error){
                        console.log(error)
                        return false
                    }else{
                        return updatedTask
                    }
                })
            }
        })
};
