// Express Server Setup
const express = require('express')
const mongoose = require('mongoose')

const taskRoutes = require('./routes/taskRoute')
const app = express()
const port = 3001;




mongoose.connect(`mongodb+srv://admin1234:admin1234@zuitt-batch197.fk6bhtr.mongodb.net/S36?retryWrites=true&w=majority`,
{
    useNewUrlParser: true,
    useUnifiedTopology: true
}
)

let db = mongoose.connection;

db.on('error',() => console.error('Connection error'));
db.once('open', () => console.log('Connection to MongoDb'));




// Middlewares
app.use(express.json())
app.use(express.urlencoded({extended: true}));

app.use('/tasks', taskRoutes)


app.listen(port, () => console.log(`Server is running at port:${port}.`))
