const express = require('express');
const TaskController = require('../controller/TaskController')
const router = express.Router();

// Routes

// ACTIVITY
router.get('/:id',(req,res) => {
    TaskController.getTask(req.params.id)
        .then((resultFromController) => res.send(resultFromController))
})

router.put('/:id/complete',(req,res) => {

    TaskController.completeTask(req.params.id, req.body)
        .then((resultFromController) => res.send(resultFromController))
})



module.exports = router;